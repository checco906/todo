<?php 

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

use Philo\Blade\Blade;

	function view  ($view, $parameters = []) {

		$views = __DIR__ . '/../views';
		$cache = __DIR__ . '/cache';

		$blade = new Blade($views, $cache);
		// echo $blade->view()->make('hello')->render(); sostituisco
		return $blade->view()->make($view, $parameters)->render();
	}

//function user() {
//return Autenticazione::getUtente();
//}
//
//function redirect($uri) {
//    header("Location:{$uri}");
//    exit;
//}
//
//function onlyAdmin() {
//    if(!user()->admin) {
//        redirect('index.php');
//    }
//}
