<?php
class Model {
    function __construct($attr=[]) {
        foreach($attr as $n=>$v) {
            $this->$n=$v;
        }
        
    }
     static function Select($id=null) {
        
        $sql = "SELECT * FROM ".static::$table;
        
        if($id) {
            $sql .= " WHERE id={$id}";
            
            
        } 
//   dd($sql);
        $res = Db::query($sql);
        //dentro una classe possiamo inserire anche delle costanti
        //pdo ha le sue costanti E si scrive con PDO::(nome costante)
        $instanze = [];
        while($row=$res->fetch(PDO::FETCH_ASSOC)) {
        $classe= get_called_class();
        $instanza = new $classe($row);
            //immagazzianare tutte le instanze all 'interno dell'array
        $instanze[] = $instanza;
        }
        if(!$id) {
            return $instanze;
        }
        //se la condizione è vera tutto il blocco ritorna NULL(prima dei due punti)
        // se la condizione è falsa ritorna la condizone dopo i due punti
        return empty($instanze[0])?null:$instanze[0];
    }
    
    function save() {
        $attrs = $this->getObjectVars();
//        $sql = "INSERT INTO domande(testo, risposta1, risposta2, risposta3, risposta4,risposta_corretta) VALUES('$this->testo', '$this->risposta1','$this->risposta2', '$this->risposta3', '$this->risposta4', '$this->risposta_corretta')";
        $sql = empty($this->id) ? 'INSERT INTO ' : 'UPDATE ';
        $sql .= static::$table. ' SET ';
        $values = [];
        foreach($attrs as $nome=>$valore){
        $valore_esc = Db::getInstance()->quote($valore);
        $values[] = "{$nome} = {$valore_esc}";
        }
        $sql .= implode(', ',$values);
        
        if(!empty($this->id)) {
            $sql .= " WHERE id = {$this->id}";
        }
    //istanza PDO per richiamare il database;
    Db:: query($sql);
        
        
    }
    static function cancella($id)
		{
			
			$sql = "DELETE FROM ".static::$table." WHERE id = {$id}";

			Db::query($sql);
			
		}
    //questo metodo acetterà un array associativo lo facciamo ciclare solo sugli attributi effettivi della classe
    function fill($attr = []) {
        $attrs = $this->getObjectVars();
        
        foreach($attr as $nome => $valore) {
           if(!array_key_exists($nome, $attrs)) {
               continue;
           } 
        $this->$nome = $valore;
        
        }
        
    }
    function getObjectVars() {
        return get_object_vars($this);
    }
}
//(abstract evita che classe venga instanziata)
//abstract class Model
//il metodo astratto:
//abstract (protected/public)function NomeFunzione();