<?php $__env->startSection('content'); ?>

	
<section class="container" style="margin-top: 50px;">
	<table class=" table is-striped table is-hoverable is-fullwidth">
		<thead>
			<tr>
				<th>ID</th>
				<th>Titolo</th>
				<th>Descrizione</th>
				<th>Data Di Scadenza</th>
				<th>Fatto</th>
				<th>Modifica</th>
				<th>Cancella</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		<?php $__currentLoopData = $todo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $todos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr>
				<td><?php echo e($todos->id); ?></td>
				<td><?php echo e($todos->titolo); ?></td>
				<td><?php echo e($todos->descrizione); ?></td>
				<td><?php echo e($todos->data_di_scadenza); ?></td>
				<td><?php echo e($todos->fatto); ?></td>
				<td><a href="form-todo.php?id=<?php echo e($todos->id); ?>">Modifica</a></td>
                <td><a href="index.php?id=<?php echo e($todos->id); ?>" onClick="return confirm(\"Sei sicuro?\");">Cancella</a>
                </td>
			</tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</tbody>
	</table>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>