<?php $__env->startSection('content'); ?>
<form method="post" accept-charset="utf-8">
 <div class="field">
  <label class="label">Titolo</label>
  <div class="control">
    <input class="input" type="text" name="titolo"  value="<?php echo e($instanza->titolo); ?>">
  </div>
</div>

<div class="field">
  <label class="label">Descrizione</label>
  <div class="control">
    <textarea class="textarea" name="descrizione" placeholder="Descrzione" value="<?php echo e($instanza->descrizione); ?>" ></textarea>
  </div>
</div>
<div class="field">
  <label class="label">Data Di Scadenza</label>
  <div class="control">
    <input class="input" type="date" name="data_di_scadenza"  value="<?php echo e($instanza->data_di_scadenza); ?>">
  </div>
</div>
<div class="field">
  <div class="control">
   <label class="label">Fatto</label>
    <label class="radio">
      <input type="radio" name="fatto" value="1" <?php echo e($instanza->fatto==1 ? 'checked' : ''); ?>>
      Si
    </label>
    <label class="radio">
      <input type="radio" name="fatto" value="0" <?php echo e($instanza->fatto==0 ? 'checked' : ''); ?>>
      No
    </label>
  </div>
</div>
<br>
<br>
<div class="control">
 <input type="hidden" name="submitted" value="1">
  <button class="button is-primary">Inserisci</button>
</div>
</form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('_template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>