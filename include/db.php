<!--classe singleton metodologia per recuperare sempre la stessa sitanza della classe-->
<?php
class Db {
    static private $instance;
    
    function __construct($database,$username,$password,$host = '127.0.0.1') {
        $dns = "mysql:dbname=$database;host=$host"; 
        try{ 
            self:: $instance = new PDO($dns, $username, $password);
        } catch(Exception $e) {
            exit($e -> getMessage());
        } 
    }

static function getInstance() {
    return self::$instance;
        
}
 static function query($sql) { 
        try{ 
           $r = Db:: getInstance()-> query($sql);
            
        } catch(PDOException $e) {
            exit($e -> getMessage());
            
        } 
     if(!$r) {
         dd(self::getInstance()->errorInfo());
     }
    return $r;
 }  
    
}

