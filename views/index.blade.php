@extends ('_template')

@section ('content')

	
<section class="container" style="margin-top: 50px;">
	<table class=" table is-striped table is-hoverable is-fullwidth">
		<thead>
			<tr>
				<th>ID</th>
				<th>Titolo</th>
				<th>Descrizione</th>
				<th>Data Di Scadenza</th>
				<th>Fatto</th>
				<th>Modifica</th>
				<th>Cancella</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		@foreach($todo as $todos)
			<tr>
				<td>{{$todos->id}}</td>
				<td>{{$todos->titolo}}</td>
				<td>{{$todos->descrizione}}</td>
				<td>{{$todos->data_di_scadenza}}</td>
				<td>{{$todos->fatto}}</td>
				<td><a href="form-todo.php?id={{$todos->id}}">Modifica</a></td>
                <td><a href="index.php?id={{$todos->id}}" onClick="return confirm(\"Sei sicuro?\");">Cancella</a>
                </td>
			</tr>
        @endforeach
		</tbody>
	</table>
</section>
@endsection